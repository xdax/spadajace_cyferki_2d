﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Cyferka : MonoBehaviour
{
    [SerializeField] Sprite spriteSplash;
    [SerializeField] Sprite[] cyferkiSprite;
    SoundManager sm;
    Parzystosc parz;
    //GameObject spadajacaCyferka;
    GameManager gm;
    readonly float predkoscSpadania = 100f;
    RectTransform rt;
    Animator anim;
    int wybranaLiczba;

    void Awake()
    {
        
        anim = GetComponent<Animator>();
        gm = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        parz = GameObject.FindGameObjectWithTag("GameManager").GetComponent<Parzystosc>();
        sm = GameObject.FindGameObjectWithTag("SM").GetComponent<SoundManager>();
        rt = GetComponent<RectTransform>();
    }
    void Start()
    {

        //dostanie sie do childa po przez obiekt GameObjecy (przypisany w unity)
        //spadajacaCyferka = gameObject.transform.GetChild(0).gameObject;
        PrzypiszWartoscNumery();
        
    }


    void PrzypiszWartoscNumery()
    {
        int losowaLiczba = Random.Range(1, 7);
        wybranaLiczba = losowaLiczba;
        GetComponent<Image>().sprite = cyferkiSprite[losowaLiczba - 1];
        //dostanie się do komementu text i przypisanie losowej liczby
        //spadajacaCyferka.GetComponent<Text>().text = losowaLiczba.ToString();

    }
    public void WyswietlLiczbe()
    {
        //przypisanie wartosci txt do zmiennej i przekonertowanie wszystkiego z stringa na int za pomoca system.int32.parse
        //int liczba = System.Int32.Parse(spadajacaCyferka.GetComponent<Text>().text);
        gm.DodajPkt(wybranaLiczba);
        AktyuwujSplasha();
        anim.SetTrigger("Znikanie");
        ZmianaColoru();
        GetComponent<Button>().interactable = false;
        parz.CyferkaPoKliknieciu(wybranaLiczba);
        sm.OdtwarzajDzwieki(0);
        UsunCyferke(0.5f);
    }
    void SpadajwDol()
    {
        //dostanie sie do komponentu pozycji obiektu i odjecie nowej pozycji Y aby uzyskac efekt spadania
        rt.anchoredPosition -= new Vector2(0f, 1f) * predkoscSpadania * Time.deltaTime;


    }
    void ZmianaColoru()
    {
        //pobranie kolorow z componentu button (wszystkich)
        ColorBlock blockKolorow = GetComponent<Button>().colors;

        //wybierz liczbe i switch decyduje co nizej z nia ma byc
        switch (wybranaLiczba)
        {
            //zmien kolor w zaleznosci od numeru
            case 1:
                blockKolorow.disabledColor = Color.red;
                break;
            case 2:
                blockKolorow.disabledColor = Color.yellow;
                break;
            case 3:
                blockKolorow.disabledColor = Color.green;
                break;
            case 4:
                //dodanie nowego koloru i prypisanie wartosci z rgb
                blockKolorow.disabledColor = new Color(0.93f, 0.78f, 1, 1) ;
                break;
            case 5:
                blockKolorow.disabledColor = Color.blue;
                break;
            case 6:
                blockKolorow.disabledColor = new Color(0.24f, 0.32f, 0.77f, 1);
                break;
            default:
                blockKolorow.disabledColor = Color.cyan;
                break;

        }
        //zniemiony kolor przypisany ponownie do blokucolorow
        GetComponent<Button>().colors = blockKolorow;
    }
    void Update()
    {
        SpadajwDol();
        if (rt.anchoredPosition.y < -375)
        {
            UsunCyferke(0f);
        }
    }
    void UsunCyferke(float time)
    {
        Destroy(gameObject, time);
    }
    void AktyuwujSplasha()
    {
        GetComponent<Image>().sprite = spriteSplash;
    }
}
