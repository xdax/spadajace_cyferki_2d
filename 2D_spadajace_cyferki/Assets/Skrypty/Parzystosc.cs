using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Parzystosc : MonoBehaviour
{
    [SerializeField] Text warunekTxt;
    bool jakaNastepnaJestLiczba;
    bool czyLiczbaJestParzysta;
    int przypisanaCyferka;
    // Start is called before the first frame update
    void Start()
    {
        ZmianaWarunkuParzyste();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void ZmianaWarunkuParzyste()
    {
        if (GetComponent<GameManager>().ZwrocRoznicePkt() == 1)
        {
            jakaNastepnaJestLiczba = false;
        }
        else
        {
            int losowaLiczba = Random.Range(0, 2);
            if (losowaLiczba == 0)
            {
                jakaNastepnaJestLiczba = true;
                warunekTxt.text = "Parzysta";
                warunekTxt.color = Color.white;
            }
            else
            {
                jakaNastepnaJestLiczba = false;
                warunekTxt.text = "Nieparzysta";
                warunekTxt.color = Color.yellow;
            }
        }

    }
    public void CyferkaPoKliknieciu(int cyferka)
    {
        przypisanaCyferka = cyferka;
        SprawdzKlikniecie();
    }
    void SprawdzKlikniecie()
    {
        if (przypisanaCyferka % 2 == 0)
        {
            czyLiczbaJestParzysta = true;
        }
        else
        {
            czyLiczbaJestParzysta = false;
        }
        if (czyLiczbaJestParzysta == jakaNastepnaJestLiczba)
        {
            ZmianaWarunkuParzyste();
        }
        else
        {
            GetComponent<GameManager>().GameOver();
        }

    }
}
