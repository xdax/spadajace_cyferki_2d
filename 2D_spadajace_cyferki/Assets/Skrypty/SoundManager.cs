using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    AudioSource audios;
    [SerializeField] AudioClip[] soudsList;
    private void Awake()
    {
        //znalezienie soud managera
        GameObject[] sm = GameObject.FindGameObjectsWithTag("SM");
        //jesli ilosc sound managerow z tabliscy (ilosci lenght) jest wiecej ni 1 to zniszcz
        if (sm.Length > 1)
        {
            Destroy(gameObject);
        }
        //nie niszcz obiektu po zaladowaniu sceny/zmieniaj
        DontDestroyOnLoad(gameObject);
    }
    // Start is called before the first frame update
    void Start()
    {
        audios = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void OdtwarzajDzwieki(int numerDzwieku)
    {
        audios.clip = soudsList[numerDzwieku];
        audios.Play();
    }
}
