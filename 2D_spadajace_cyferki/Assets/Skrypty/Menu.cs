﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    private void Start()
    {
        //pobierz aktualna scene z indexem i porwnaj czy jest to 1
        if (SceneManager.GetActiveScene().buildIndex == 1)
        {
            GameObject.FindGameObjectWithTag("SM").GetComponent<SoundManager>().OdtwarzajDzwieki(2);
        }
        else if ((SceneManager.GetActiveScene().buildIndex == 3))
        {
            GameObject.FindGameObjectWithTag("SM").GetComponent<SoundManager>().OdtwarzajDzwieki(1);
        }
    }

    public void Play()
    {
        SceneManager.LoadScene(2);
    }
    public void Quit()
    {
        Application.Quit();
    }
    public void MenuGlowne()
    {
        SceneManager.LoadScene(0);
    }
}
