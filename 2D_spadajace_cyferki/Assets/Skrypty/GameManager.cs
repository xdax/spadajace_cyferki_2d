﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField] GameObject cyferkaPrefab;
    [SerializeField] Transform kontenerCyferki;
    [SerializeField] Text liczbaDoceolowaTxt;
    [SerializeField] Text liczbaPunktowTxt;
    [SerializeField] Text licznikCzasuTxt;

    Coroutine co;

    float czasStartowy = 30f;
    float licznikCzasu;

    int liczbaDocelowa = 21;
    int liczbaPkt = 0;
    void Start()
    {

        co = StartCoroutine(TworzenieCyferki());
        liczbaDoceolowaTxt.text = liczbaDocelowa.ToString();
        licznikCzasu = czasStartowy;

    }
    public int ZwrocRoznicePkt()
    {
        return liczbaDocelowa - liczbaPkt;
    }
    void ZmianaKoloruCzasu()
    {
        if (licznikCzasu <= 5)
        {
            licznikCzasuTxt.color = Color.red;
        }
        else if (licznikCzasu <= 10)
        {
            licznikCzasuTxt.color = Color.yellow;
        }
    }

    void AktualizacjaUI()
    {
        liczbaPunktowTxt.text = liczbaPkt.ToString();
    }
    IEnumerator TworzenieCyferki()
    {
        //tworznie obiektu
        GameObject nowaCyferka = Instantiate(cyferkaPrefab, kontenerCyferki);
        //stworzenie zmiennej z randomowa pozycja
        int losowaPozycjaX = Random.Range(-130, 131);
        //pobranie pozycji i dodanie losowej
        cyferkaPrefab.GetComponent<RectTransform>().anchoredPosition = new Vector2(losowaPozycjaX, 260f);
        //odliczanie czasu
        yield return new WaitForSeconds(1f);
        //dodanie fukcji aby powtrzajac ja caly czas
        co = StartCoroutine(TworzenieCyferki());

    }

    // Update is called once per frame
    void Update()
    {
        AktualizacjaUI();
        OdliczanieCzasu();
        SprawdzStatusGry();
    }
    public void DodajPkt(int punkt)
    {
        liczbaPkt += punkt;
    }
    void OdliczanieCzasu()
    {
        if (czasStartowy > 0)
        {
            licznikCzasu -= Time.deltaTime;
            licznikCzasuTxt.text = licznikCzasu.ToString("f2");
            ZmianaKoloruCzasu();
        }
        else if (czasStartowy == 0)
        {
            GameOver();
        }
 
    }
    void WinTheGame()
    {
        SceneManager.LoadScene(0);
    }
    public void GameOver()
    {
        SceneManager.LoadScene(1);

    }
    void SprawdzStatusGry()
    {
        if (licznikCzasu > 0)
        {
            if (liczbaPkt == liczbaDocelowa)
            {
                WinTheGame();
            }
            else if (liczbaPkt > liczbaDocelowa)
            {
                GameOver();
            }
        }
        else
        {
            GameOver();
        }
         

    }
}
